package com.fire.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {

	XSSFWorkbook wb;
	XSSFSheet sheet1;

	public ExcelDataConfig(String excelpath)   
	{
		// TODO Auto-generated method stub

		try {
			File src = new File(excelpath);
			FileInputStream fis = new FileInputStream(src);
			wb = new XSSFWorkbook(fis);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

	public String getData(int sheetnumber, int row, int column) throws IOException
	{
		sheet1 = wb.getSheetAt(sheetnumber);
		String Data = sheet1.getRow(row).getCell(column).getStringCellValue();
		return Data;	

	}

	public int getrowcount(int sheetnumber)
	{
		sheet1 = wb.getSheetAt(sheetnumber);
		int row = sheet1.getLastRowNum(); 
		int rownum = row;
		return rownum;

	}

}