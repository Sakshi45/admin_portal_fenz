package com.fire.utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class BaseClass {

	
	String url = "https://as-mobility-admin-portal-s01.azurewebsites.net";
	public static WebDriver driver;
	
	

	public void startbrowser() {
		
		
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability("marionette", true);
			driver = new ChromeDriver();	
	}
	
	public void getUrl(String url) {

				driver.get(url);
				driver.manage().window().maximize();

			}

public void init() throws Exception
			{
				startbrowser();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				getUrl(url);

			}
		

	}

	
	
